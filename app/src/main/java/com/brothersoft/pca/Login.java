package com.brothersoft.pca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewAnimator;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try   // PCA_Config Custom Back Ground for Action bar
        {
            android.app.ActionBar bar = getActionBar();
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.img_header));
            //bar.setlogo(R.drawable.icnabout);
            checkRegistration();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    } //End onCreat


    private void checkRegistration()
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this);
        String registeredEmail = sharedPreferences.getString("registeredEmail", "Not_Set");

        if(registeredEmail.equals("Not_Set"))
        {

            Intent registerIntent = new Intent(Login.this ,Registeration.class);
            startActivity (registerIntent);
            finish();
        }
        Toast.makeText(getApplicationContext(), registeredEmail, Toast.LENGTH_LONG).show();
    }//end checkRegistration()

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_login_now:

                if(checkCredentials())
                {
                    Intent homeIntent = new Intent(Login.this , Home.class);
                    startActivity (homeIntent);
                    finish();
                }

                else
                {
                    Toast.makeText(getApplicationContext(), "Sorry! Credentials are invalid!! \n Please Try again" , Toast.LENGTH_LONG).show();

                }

                break;

           // case R.id.btn_exit:
           //     finish();
           //     break;

            case R.id.tv_forget_info:
                Intent forgotIntent = new Intent(Login.this , ForgotCredential.class);
                startActivity(forgotIntent);
                finish();
            default:
                break;
        } //End of Switch
    } //End onClick(view view)

    private boolean checkCredentials()
    {
        EditText etEmail    = (EditText) findViewById(R.id.et_email_login);
        EditText etPassword = (EditText) findViewById(R.id.et_password_login);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Login.this);

        //Retrieving preferences for email & password
        String email 	= sharedPreferences.getString("registeredEmail", "not set");
        String password = sharedPreferences.getString("registeredPassword", "not set");

        if(email.equals("not set") || password.equals("not set"))
        {
            return false;

        }else if(email.equals(etEmail.getText().toString()) && password.equals(etPassword.getText().toString()))
        {
            return true;
        }
        return false;
    }//end checkCredentials()

}
