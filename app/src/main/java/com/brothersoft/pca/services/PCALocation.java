package com.brothersoft.pca.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PCALocation
{
	//fields
	private long imei;
	private String time;
	private double latitude;
	private double longitude;
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//default constructor
	public PCALocation() 
	{
		setImei(0000000);
		setTime(showCurrentTime());
		setLatitude(0.0000);
		setLongitude(0.0);
	}

	public long getImei() 
	{
		return imei;
	}

	public void setImei(long imei) 
	{
		this.imei = imei;
	}

	public String getTime() 
	{
		return time;
	}

	public void setTime(String time) 
	{
		this.time = time;
	}

	public double getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}

	public double getLongitude() 
	{
		return longitude;
	}

	public void setLongitude(double longitude) 
	{
		this.longitude = longitude;
	}
	
	//show current time of system
	public String showCurrentTime() 
	{		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}//end showCurrentTime()
	
	
	public String toString()
	{
		return this.getId() + " - " + this.getImei() + " - " + this.getTime() + " - " + this.getLatitude() + " - " + this.getLongitude();
		
	}//end toString()
	

}//end MtaafLocation
