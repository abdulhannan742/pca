package com.brothersoft.pca.services;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;



import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;


public class TrackingService extends Service 
{
	//Global Variables
	private final Handler myHandler = new Handler();
	private GPSTracker gpsTracker;
	private TimerTask timerTask;	
	private SharedPreferences sharedPreferences;
	private Timer trackingTimer;
	private final Handler trackingHandler = new Handler();
	
	public TrackingService() 
	{
		// TODO Auto-generated constructor stub
	}
	
	/******************************************************************************/
	/***********************   onCreate()     *************************************/
	/******************************************************************************/
	@Override
	public void onCreate ()
	{
		super.onCreate();		
		//Log.e("Tracking Service", "onCreate - started");
		/************     start code for Timer      ********************************/
		trackingTimer = new Timer();	
		//trackingHandler = new Handler();
	    
	    TimerTask timerTask = new TimerTask() 
	    {
	         @Override
	         public void run() 
	         {
	        	 //Log.e("Tracking Service", "TimerTask run - started");
	        	 UpdateDB();
	         }//end run()			

	      };//end definition of timerTask variable
	      
	    trackingTimer.schedule(timerTask, 0, 60000);    //1 minute(60 seconds) interval
	      
	    
	      
	    /************     end code for Timer      ********************************/	      	       
	        
	        //Location to be saved into the remote database
	    //mtaafLocation = new MtaafLocation();            //Creating a new location Object 
	        //mtaafLocation = 
	    //databaseAdapter = new MtaafLocationDbAdapter(getApplicationContext());   	      
		
	}//end onCreate()
	
	@Override
	public void onDestroy() 
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		if(trackingTimer != null)
		{
			trackingTimer.cancel();
			Log.e("Tracking Service", "Timer Canclled");
		}
		
		Log.e("Tracking Service", "Destroyed");
	}

	public Location getCurrentLocation() 
	{
		
		//myCurrentLocation.setLatitude(21.42);
		
		// create class object
		gpsTracker = new GPSTracker(getApplicationContext());  //call to constructor sets the current location
		//Log.e("gpsTracker.getLatitude()", gpsTracker.getLatitude() + "-" + gpsTracker.getLongitude());
		Location myCurrentLocation = new Location("Lahore");
		
		// check if GPS enabled
		if(gpsTracker.canGetLocation())
		{
			myCurrentLocation.setLatitude(gpsTracker.getLatitude());
			myCurrentLocation.setLongitude(gpsTracker.getLongitude());
			//Log.e("gpsTracker.getLatitude()", myCurrentLocation.getLatitude() + "-" + myCurrentLocation.getLongitude());

		}else
		{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings			
			Log.e("GPS", "GPS is off");
			return null;
		}//end if-else	*/			
		return myCurrentLocation;			
		
	}
	
	
	private void UpdateDB() 
	{
		//seconds++;
		Log.e("Tracking Service", "UpdateDB() - started");
		try
		{
			Log.e("Tracking Service", "Location: Latitude = " + getCurrentLocation().getLatitude() + " Longitude = " + getCurrentLocation().getLongitude());	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		trackingHandler.post(myRunnable);
		
	}
	
	final Runnable myRunnable = new Runnable() 
	{
		
		@Override
		public void run() 
		{
			/*
			if(checkNetwork())
			{
							
		        
				/**************************************  Getting all records from the database ************************/
				/*databaseAdapter.open();
				ArrayList<MtaafLocation> records = databaseAdapter.fetchAllRecords();
							
				
				int row = 0;
				for(row = 0; row < records.size(); row++)
				{
					mtaafLocation = new MtaafLocation();            //Creating a new location Object
					mtaafLocation = records.get(row);
					
					// Calling async task to get json
			        new SaveLocations().execute(mtaafLocation.getImei() + "", mtaafLocation.getTime(), mtaafLocation.getLatitude() + "", mtaafLocation.getLongitude() + "");
			        
				}
				databaseAdapter.deleteAllRecords(row);
		        databaseAdapter.close();

			}
			*/
			
		}//end run()
	};//end definition of myRunnable variable
	
	/************     End code for      ********************************/	
	

	@Override
	public IBinder onBind(Intent intent) 
	{
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TrackingService.this);
		
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		   super.onStartCommand(intent, flags, startId);
		   //Log.e("Tracking Service", "onStartCommand - started" + intent + flags + startId);
		   return START_STICKY;
	}	
}//end Service