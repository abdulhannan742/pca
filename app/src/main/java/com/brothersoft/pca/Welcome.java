package com.brothersoft.pca;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Welcome extends AppCompatActivity
{

    //Time out for Welcome/Start up Screen
    private static int SPLASH_TIME_OUT = 3000;

    /*******************************************************/
    /***************   onCreate()   ************************/
    /*******************************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        /*******************  Splash Code              ************/
        //Handler myHandler;
        Handler myHandler =  new Handler();
        myHandler.postDelayed(new Runnable()
        {
            /*
             * Showing splash screen with a timer.
             */
            public void run()
            {
                // This method will be executed once the timer is over
                // Start the application's Home activity
                Intent goToHome = new Intent(Welcome.this, Login.class);
                startActivity(goToHome);

                // close Splash activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }//end onCreate()



}
