package com.brothersoft.pca.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class TrackingDbOpenHelper extends SQLiteOpenHelper 
{
	//Database
	private static final String DATABASE_NAME = "TrackingDB";
	private static final int DATABASE_VERSION = 1;

	//Database Table
	public static final String TABLE_NAME              = "TrackingLocations";
	    
	//Columns
	public static final String COLUMN_NAME_ID = "_id";
	public static final String COLUMN_NAME_IMEI        = "imei";
	public static final String COLUMN_NAME_TIME        = "time";
	public static final String COLUMN_NAME_LATITUDE    = "latitude";
	public static final String COLUMN_NAME_LONGITUDE   = "longitude";	    	

	public TrackingDbOpenHelper(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}



	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		String createSql = "CREATE TABLE " + TABLE_NAME +
	            "( "  + COLUMN_NAME_ID + " integer primary key autoincrement, " + 
				COLUMN_NAME_IMEI + " integer, " +
				COLUMN_NAME_TIME + " text not null, " +
				COLUMN_NAME_LATITUDE + " real not null, " +
				COLUMN_NAME_LONGITUDE + " real not null);";
	            //"name text not null
	    db.execSQL(createSql);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	    onCreate(db);

	}

}
