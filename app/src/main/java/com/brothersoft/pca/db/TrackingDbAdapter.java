package com.brothersoft.pca.db;

import java.util.ArrayList;
import com.brothersoft.pca.services.PCALocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TrackingDbAdapter 
{
	private Context context;
	private SQLiteDatabase database;
	private TrackingDbOpenHelper dbHelper;
	public static final String[] allColumns = new String[] { TrackingDbOpenHelper.COLUMN_NAME_ID, TrackingDbOpenHelper.COLUMN_NAME_IMEI, TrackingDbOpenHelper.COLUMN_NAME_TIME, TrackingDbOpenHelper.COLUMN_NAME_LATITUDE, TrackingDbOpenHelper.COLUMN_NAME_LONGITUDE};
	 

	public TrackingDbAdapter(Context context) 
	{
		this.context = context;
	}//end constructor
	
	
	public TrackingDbAdapter open() throws SQLException 
	{
	    dbHelper = new TrackingDbOpenHelper(context);
	    database = dbHelper.getWritableDatabase();
	    return this;
	}//end open
	
	public void close() 
	{
	    dbHelper.close();
	}//end close()
	
	public long createRecord(long imei, String time, double latitude, double longitude) 
	{
	    ContentValues contentValue = new ContentValues();
	    contentValue.put(TrackingDbOpenHelper.COLUMN_NAME_IMEI, imei);
	    contentValue.put(TrackingDbOpenHelper.COLUMN_NAME_TIME, time);
	    contentValue.put(TrackingDbOpenHelper.COLUMN_NAME_LATITUDE, latitude);
	    contentValue.put(TrackingDbOpenHelper.COLUMN_NAME_LONGITUDE, longitude);
	    
	    return database.insert(TrackingDbOpenHelper.TABLE_NAME, null, contentValue);
	}//end createRecoed
	
	public ArrayList<PCALocation> fetchAllRecords() 
	{   
		Cursor cursor = database.query(TrackingDbOpenHelper.TABLE_NAME, allColumns, null, null, null, null, null);        
		ArrayList<PCALocation> records = new ArrayList<PCALocation>();        
	    cursor.moveToFirst();
	    for (int i = 0; i < cursor.getCount(); i++) 
	    {
	    	//must create inside the loop to be added in the ArrayList
	    	PCALocation pcaLocation = new PCALocation();

	    	//Making Location with db data
	    	pcaLocation.setId(cursor.getString(0));
	    	pcaLocation.setImei(cursor.getLong(1));
	    	pcaLocation.setTime(cursor.getString(2));
	    	pcaLocation.setLatitude(cursor.getDouble(3));
	    	pcaLocation.setLongitude(cursor.getDouble(4));	    	
	    	
	    	//adding location object in the ArrayList
	        //records.add(mtaafLocation);
	        records.add(i, pcaLocation);
	        cursor.moveToNext();
	    }//end for
	    cursor.close();
	    return records;
	    
	}//end fetchAllRecords
	
	public void deleteAllRecords(int numberOfRows) 
	{

		database.delete(TrackingDbOpenHelper.TABLE_NAME, null, null);
		
	}
}//end class