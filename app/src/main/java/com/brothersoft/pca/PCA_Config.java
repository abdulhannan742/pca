package com.brothersoft.pca;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.brothersoft.pca.services.GPSTracker;
import com.brothersoft.pca.services.TrackingService;

public class PCA_Config extends AppCompatActivity
{
    public static final String TRACKING_STATUS = "trackingSwitch";
    public static final String TRACKING_ON = "on";
    public static final String TRACKING_OFF = "off";
    Switch trackingSwitch;
    SharedPreferences sharedPreferences;
    private GPSTracker gpsTracker;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        try   // PCA_Config Custom Back Ground for Action bar
        {
            android.app.ActionBar bar = getActionBar();
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.img_header));
            //bar.setlogo(R.drawable.icnabout);

            checkTrackingStatus();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    //Check Tracking Switch State
    private void checkTrackingStatus()
    {
        trackingSwitch = (Switch) findViewById(R.id.tracking_switch);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(PCA_Config.this);
        String status = sharedPreferences.getString(TRACKING_STATUS, "not set");
        if(status.equals(TRACKING_ON))
        {
            trackingSwitch.setChecked(true);
            trackingSwitch.setText("Tracking is on");
        }else
        {
            trackingSwitch.setChecked(false);
            trackingSwitch.setText("Tracking is off");
        }

        //Attach a listener to check the state of switch
        trackingSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                //Service to be started as switch turns to ON (Tracking Service)
                Intent startTracking = new Intent(PCA_Config.this, TrackingService.class);
                if (isChecked) //If Switch ON
                {
                    sharedPreferences.edit().putString(TRACKING_STATUS, TRACKING_ON).commit();

                    gpsTracker = new GPSTracker(getApplicationContext());  //call to constructor sets the current location
                    if(gpsTracker.canGetLocation())
                    {
                        startService(startTracking);
                        trackingSwitch.setText("Tracking is on");
                        trackingSwitch.setChecked(true);
                    }else
                    {
                        trackingSwitch.setChecked(false);
                        showSettingsAlert();
                    }

                } else
                {
                    sharedPreferences.edit().putString(TRACKING_STATUS, TRACKING_OFF).commit();
                    if(startService(startTracking) != null)
                    {
                        stopService(startTracking);
                    }

                    trackingSwitch.setText("Tracking is off");
                }

            }

        });

    }//end checkTrackingStatus()

    /********************************************************/
    /************* Showing Settings Alert  ******************/
    /********************************************************/
    public void showSettingsAlert()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // PCA_Config Dialog Title
        alertDialog.setTitle(R.string.gps_message_title);

        // PCA_Config Dialog Message
        alertDialog.setMessage(R.string.gps_message);

        // PCA_Config Icon to Dialog
        alertDialog.setIcon(R.drawable.ic_launcher);

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.gps_positive_button, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.gps_negative_button, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }//end showSettings

}
